import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import logic.CommonDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import web.Command;
import web.Poll;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import persistence.PersistenceManager;

public class PeetooHeart {

	private static Logger LOG = LoggerFactory.getLogger(PeetooHeart.class);

	public static void main(String[] args) {

		try {
			PersistenceManager pm = new PersistenceManager();

			final Poll poll = new Poll();
			final CommonDispatcher ad = new CommonDispatcher();
			while (true) {
				final String offset = pm.getString("offset");
				final Command command = poll.doPoll(offset);
				if (command != null) {
					final List<String> responses = ad.dispatch(command);
					for (final String response : responses) {
						sendMessage(response);
					}
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
			main(args);
		}
	}

	private static void sendMessage(String value) throws IOException {
		String url = "https://api.telegram.org/bot230014010:AAE-VOBPU30n29c07KkiL7UnEjoGlaT8WEY/sendMessage";

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost request = new HttpPost(url);

		List<NameValuePair> urlParameters = new ArrayList<>();
		urlParameters.add(new BasicNameValuePair("chat_id", "-1001061384829"));

		urlParameters.add(new BasicNameValuePair("text", value));

		request.setEntity(new UrlEncodedFormEntity(urlParameters));

		// add request header
		HttpResponse response = client.execute(request);

		System.out.println("Response Code : "
				+ response.getStatusLine().getStatusCode());
	}
}
