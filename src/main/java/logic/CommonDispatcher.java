package logic;

import component.rss.Dispatcher;
import component.rss.RssDispatcher;
import web.Command;

import java.util.Collections;
import java.util.List;

public class CommonDispatcher implements Dispatcher {

	private Dispatcher rssd = new RssDispatcher();

	@Override
	public List<String> dispatch(final Command command) {

		if (command.getCommand().startsWith("rss")) {

			return rssd.dispatch(command);
		} else {

			return Collections.singletonList("SUka ne pishi hujniu!!!!");
		}
	}
}
