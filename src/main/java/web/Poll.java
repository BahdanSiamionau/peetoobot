package web;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import persistence.PersistenceManager;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Poll {

	private static Logger LOG = LoggerFactory.getLogger(Poll.class);

	private PersistenceManager pm = new PersistenceManager();

	public Command doPoll(final String offset) {

		String url = "https://api.telegram.org/bot230014010:AAE-VOBPU30n29c07KkiL7UnEjoGlaT8WEY/getUpdates";

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost request = new HttpPost(url);

		List<NameValuePair> urlParameters = new ArrayList<>();
		urlParameters.add(new BasicNameValuePair("timeout", "600"));
		urlParameters.add(new BasicNameValuePair("offset", offset));

		try {
			request.setEntity(new UrlEncodedFormEntity(urlParameters));
			HttpResponse response = client.execute(request);

			return extractCommand(response.getEntity().getContent());
		} catch (java.io.IOException e) {
			throw new RuntimeException();
		}
	}

	private Command extractCommand(final InputStream is) {

		final JSONObject jsonObject = new JSONObject(convertStreamToString(is));
		if (jsonObject.getJSONArray("result").length() == 0) {
			return null;
		}

		LOG.debug(jsonObject.toString());

		final Integer updateId = jsonObject.getJSONArray("result")
				.getJSONObject(0)
				.getInt("update_id");
		updateOffset(updateId);
		final String[] rawMessage = jsonObject.getJSONArray("result")
				.getJSONObject(0)
				.getJSONObject("message")
				.getString("text")
				.split(" ");
		final String command = rawMessage[0].substring(1).split("@")[0];
		if (rawMessage.length > 1) {
			return new Command(command, Arrays.copyOfRange(rawMessage, 1, command.length() - 1));
		} else {
			return new Command(command);
		}
	}

	private String convertStreamToString(final InputStream is) {
		Scanner s = new Scanner(is).useDelimiter("\\A");
		return s.hasNext() ? s.next() : "";
	}

	private void updateOffset(final Integer updateId) {

		pm.putString("offset", updateId + 1);
	}
}
