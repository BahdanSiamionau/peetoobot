package web;

import java.util.Arrays;
import java.util.List;

public class Command {

	private String command;
	private List<String> args;

	public Command(String command) {
		this.command = command;
	}

	public Command(final String command, final String... args) {

		this.command = command;
		this.args = Arrays.asList(args);
	}

	public String getCommand() {
		return command;
	}

	public List<String> getArgs() {
		return args;
	}
}
