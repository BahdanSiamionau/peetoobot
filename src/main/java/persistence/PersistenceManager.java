package persistence;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;
import java.util.function.Function;

public class PersistenceManager {

	public String getString(final String key) {

		return get(map -> (String) map.get(key));
	}

	public void putString(final String key, final Object value) {

		put(map -> map.put(key, value.toString()));
	}

	public Date getDate(final String key) {

		return get(map -> (Date) map.get(key));
	}

	public void putDate(final String key, final Date value) {

		put(map -> map.put(key, value));
	}

	public List<String> getStringList(final String key) {

		return get(map -> (List<String>) map.getOrDefault(key, new ArrayList<>()));
	}

	public void putIntoStringList(final String key, final Object value) {

		final List<String> stringList = getStringList(key);
		if (!stringList.contains(value.toString())) {
			stringList.add(value.toString());
		}
		put(map -> map.put(key, stringList));
	}

	public void removeFromStringList(final String key, final Object value) {

		final List<String> stringList = getStringList(key);
		stringList.remove(value.toString());
		put(map -> map.put(key, stringList));
	}

	private <T> T get(Function<Map, T> function) {

		final DB db = DBMaker.fileDB("file.db").make();
		final ConcurrentMap map = db.hashMap("map").make();

		final T value = function.apply(map);
		db.close();
		return value;
	}

	private void put(Consumer<Map> consumer) {

		DB db = DBMaker.fileDB("file.db").make();
		ConcurrentMap map = db.hashMap("map").make();

		consumer.accept(map);
		db.close();
	}
}
