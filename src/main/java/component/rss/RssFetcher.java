package component.rss;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import persistence.PersistenceManager;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.BiPredicate;

public class RssFetcher {

	private RssPersistence rp = new RssPersistence();
	private PersistenceManager pm = new PersistenceManager();

	public List<String> doFetch() {

		final List<String> feeds = rp.getFeeds();
		final List<String> result = new ArrayList<>();
		for (final String feed : feeds) {
			result.addAll(singleFetch(feed));
		}
		return result;
	}

	private List<String> singleFetch(final String feed) {

		if (feed != null && !feed.isEmpty()) {
			final Date lastUpdate = pm.getDate(feed);
			if (lastUpdate == null) {
				return getPosts(feed, (lu, t) -> t > 0);
			} else {
				return getPosts(feed, (lu, t) -> lastUpdate.equals(lu) || t > 4);
			}
		} else {
			return new ArrayList<>();
		}
	}

	private List<String> getPosts(final String feed, final BiPredicate<Date, Integer> predicate) {

		try {
			List<String> result = new ArrayList<>();

			URL feedUrl = new URL(feed);
			SyndFeedInput input = new SyndFeedInput();
			SyndFeed syndFeed = input.build(new XmlReader(feedUrl));

			for (int i = 0; i < syndFeed.getEntries().size(); i++) {
				final SyndEntry se = syndFeed.getEntries().get(i);

				if (predicate.test(se.getPublishedDate(), i)) {
					break;
				}
				result.add(se.getLink());
				if (i == 0) {
					pm.putDate(feed, se.getPublishedDate());
				}
			}

			return result;
		} catch (java.io.IOException | FeedException e) {
			throw new RuntimeException(e);
		}
	}
}
