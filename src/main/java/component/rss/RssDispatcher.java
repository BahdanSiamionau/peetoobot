package component.rss;

import web.Command;

import java.util.Collections;
import java.util.List;

public class RssDispatcher implements Dispatcher {

	private RssPersistence rp = new RssPersistence();
	private RssFetcher rf = new RssFetcher();

	@Override
	public List<String> dispatch(Command command) {

		switch (command.getCommand()) {
			case "rssfeedfetch":
				return rf.doFetch();
			case "rssfeedlist":
				return Collections.singletonList(rp.getFeeds()
						.stream()
						.sorted()
						.reduce((res, s) -> res.isEmpty() ? s : res + "\n" + s)
						.orElse("net ni huja pidr"));
			case "rssfeedadd":
				if (command.getArgs() != null) {
					for (final String arg : command.getArgs()) {
						if (arg != null) {
							rp.addFeed(arg);
						}
					}
					return Collections.singletonList("Subscribed successfully!");
				} else {
					return Collections.singletonList("ti cho suka KOKO url dobav pidr");
				}
			case "rssfeedremove":
				if (command.getArgs() != null) {
					for (final String arg : command.getArgs()) {
						if (arg != null) {
							rp.removeFeed(arg);
						}
					}
				}
				return Collections.singletonList("Unsubscribed successfully!");
			default:
				return Collections.singletonList("Ty ebanytij? Net takoj komandy. Kudkudah.");
		}
	}
}
