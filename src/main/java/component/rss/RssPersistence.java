package component.rss;

import persistence.PersistenceManager;

import java.util.List;

/**
 * Created by siamionau on 28.05.16.
 */
public class RssPersistence {

	private PersistenceManager pm = new PersistenceManager();

	public List<String> getFeeds() {

		return pm.getStringList("feed");
	}

	public void addFeed(final String url) {

		pm.putIntoStringList("feed", url);
	}

	public void removeFeed(final String url) {

		pm.removeFromStringList("feed", url);
	}
}
