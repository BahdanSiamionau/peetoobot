package component.rss;

import web.Command;

import java.util.List;

public interface Dispatcher {

	List<String> dispatch(Command command);
}
